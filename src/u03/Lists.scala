package u03

import u02.Optionals.Option.None
import u02.Optionals.Option.Some
import u02.Optionals._

import scala.annotation.tailrec

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), n) if n > 0 => drop(t, n - 1)
      case _ => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

    def mapFlatMap[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(e => Cons(mapper(e), Nil()))

    def filterFlatMap[A](l1: List[A])(pred: A => Boolean): List[A] = flatMap[A, A](l1)(e => if (pred(e)) {
      Cons(e, Nil())
    } else {
      Nil[A]()
    })

    @tailrec
    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h, Cons(t, tt)) => max(Cons(Math.max(h, t), tt))
      case Cons(h, _) => Option.Some(h)
      case _ => Option.None()
    }

    def max2(l: List[Int]): Option[Int] = {
      @tailrec
      def _max(l1: List[Int], m: Option[Int]): Option[Int] = (l1, m) match {
        case (Cons(h, t), Some(x)) => _max(t, Some(Math.max(h, x)))
        case (Cons(h, t), None()) => _max(t, Some(h))
        case _ => m
      }

      _max(l, None())
    }

    @scala.annotation.tailrec
    def foldLeft[E, C](list: List[E])(d: C)(f: (C, E) => C): C = list match {
      case Cons(h, t) => foldLeft(t)(f(d, h))(f)
      case _ => d
    }

    def foldRight[E, C](list: List[E])(d: C)(f: (E, C) => C): C = list match {
      case Cons(h, t) => f(h, foldRight(t)(d)(f))
      case _ => d
    }
  }

}

object ListsMain extends App {

  import Lists._

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60

  import List._

  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >= 20)) // 20,30
}