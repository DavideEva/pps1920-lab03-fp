package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals._
import u02.SumTypes.{Student, Teacher}
import u03.Lists.List._
import u03.Streams.Stream

class ExerciseTest {

  private val lst = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testListDrop() {
    assertEquals(Cons(10, Cons(20, Cons(30, Nil()))), drop(lst, 0))
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test def testFlatMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)(v => Cons(v + 1, Cons(v + 2, Nil()))))
    assertEquals(Nil[Int](), flatMap(lst)(_ => Nil[Int]()))
  }

  @Test def testFlatMapMap(): Unit = {
    assertEquals(Cons(20, Cons(40, Cons(60, Nil()))), mapFlatMap(lst)(_ * 2))
  }

  @Test def testMax(): Unit = {
    assertEquals(Option.Some(25), max(Cons(10, Cons(25, Cons(20, Nil())))))
    assertEquals(Option.None(), max(Nil()))
    assertEquals(Option.Some(25), max2(Cons(10, Cons(25, Cons(20, Nil())))))
    assertEquals(Option.None(), max2(Nil()))
  }

  @Test def testCourses(): Unit = {
    assertEquals(Cons("Math", Nil()), SumTypes.getCourses(Cons(Teacher("Capaldi", "Math"), Cons(Student("Marco", 1920), Nil()))))
  }

  private val lst2 = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test def testFoldLeft(): Unit = {
    assertEquals(-16, foldLeft(lst2)(0)(_ - _))
    assertEquals(0, foldLeft(Nil[Int]())(0)(_ - _))
  }

  @Test def testFoldRight() {
    assertEquals(-8, foldRight(lst2)(0)(_ - _))
    assertEquals(0, foldRight(Nil[Int]())(0)(_ - _))
  }

  @Test def testDrop(): Unit = {
    var str = Stream.iterate(0)(_ + 1) // {0,1,2,3,..}
    str = Stream.map(str)(_ + 1) // {1,2,3,4,..}
    str = Stream.filter(str)(x => (x < 3 || x > 20)) // {1,2,21,22,..}
    str = Stream.take(str)(10) // {1,2,21,22,..,28}
    str = Stream.drop(str)(3)
    assertEquals(Cons(22, Cons(23, Cons(24, Cons(25, Cons(26, Cons(27, Cons(28, Nil()))))))), Stream.toList(Stream.take(str)(10)))
  }

  @Test def testConst(): Unit = {
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), Stream.toList(Stream.take(Stream.constant("x"))(5)))
  }

  @Test def testFib(): Unit = {
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))), Stream.toList(Stream.take(Stream.fib())(8)))
  }
}
